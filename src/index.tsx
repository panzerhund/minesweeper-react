import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from "react-redux";
import {createStore} from "redux";
import App from "./App";
import {rootReducer} from "./com/rexgamez/minesweeper/store/reducers/RootReducer";
import './index.css';
import * as serviceWorker from './serviceWorker';

const store = createStore(rootReducer,
    (window as any).__REDUX_DEVTOOLS_EXTENSION__ && (window as any).__REDUX_DEVTOOLS_EXTENSION__());
ReactDOM.render(
    <Provider store={store}>
        <App/></Provider>, document.getElementById('root'));


serviceWorker.unregister();
