import React from 'react';
import './App.css';
import Board from "./com/rexgamez/minesweeper/board/Board";
import './styles/_styles.scss'

class App extends React.Component {
    render() {
        return (
            <div className="App">
                <header className="App-header">
                    <Board></Board>
                </header>
            </div>
        );
    }
}

export default App;
