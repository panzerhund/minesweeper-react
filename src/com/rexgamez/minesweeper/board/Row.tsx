import React from 'react';
import {FieldData} from "../store/board/FieldData";
import Field from "./Field";

type Props = {
    row: number,
    cols: number;
    rowContents: number[],
    onFieldClicked: (p: FieldData) => void;
}

class Row extends React.Component<Props> {
    render() {
        const cols = [];
        for (let i: number = 0; i < this.props.cols; i++) {
            cols.push(<Field onClicked={(() => this.props.onFieldClicked({x: i, y: this.props.row}))}
                             key={`col${i}`}
                             content={this.props.rowContents[i]}></Field>)
        }
        return (
            <div className="row">
                {cols}
            </div>
        )
    }
}

export default Row;