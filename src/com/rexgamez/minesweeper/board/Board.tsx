import React from 'react';
import {connect} from "react-redux";
import {FieldData} from "../store/board/FieldData";
import {RevealFieldAction, revealFieldAction} from "../store/board/revealFieldAction";
import {RootStore} from "../store/reducers/RootStore";
import Row from "./Row";

type Props = {
    rows: number;
    cols: number;
    revealField: (p: FieldData) => RevealFieldAction;
    contents: number[][];
};

const mapStateToProps = (state: RootStore) => ({
    rows: state.board.rows,
    cols: state.board.cols,
    contents: state.board.fieldsContents,
});

const mapDispatchToProps = (dispatch: any, ownProps: any) => {
    return {
        revealField: (p: FieldData) => dispatch(revealFieldAction(p)),
    }
};

class Board extends React.Component<Props> {

    onBoardClicked(p: FieldData): void {
        console.log(p)
        this.props.revealField(p)
    }

    render() {
        const rows = [];
        for (let i: number = 0; i < this.props.rows; i++) {
            rows.push(
                <Row
                    row={i}
                    cols={this.props.cols}
                    rowContents={this.props.contents[i]}
                    onFieldClicked={this.onBoardClicked.bind(this)}
                    key={`row${i}`}/>)
        }
        return (
            <div>
                <p>MINESWEEPER</p>
                {rows}
            </div>

        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Board);

