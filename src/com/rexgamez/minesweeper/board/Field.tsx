import React from "react";
import {FieldContent} from "../store/board/field/Fields";

type Props = {
    key: string;
    content: FieldContent;
    onClicked: () => void;
}

class Field extends React.Component<Props> {

    render() {
        return (<div onClick={() => this.props.onClicked()}
                     className={"field "+(this.props.content === FieldContent.REVEALED ? "field-revealed" : "")}>
        </div>)
    }
}

export default Field;