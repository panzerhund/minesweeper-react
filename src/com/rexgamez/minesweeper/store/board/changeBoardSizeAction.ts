import {Action} from "redux";
import {FieldData} from "./FieldData";

export const CHANGE_BOARD_SIZE_ACTION = 'CHANGE_BOARD_SIZE_ACTION';

export interface ChangeBoardSizeAction extends Action {
    size: FieldData;
}

export function changeBoardSizeAction(size: FieldData): ChangeBoardSizeAction {
    return {
        type: CHANGE_BOARD_SIZE_ACTION,
        size: size,
    }
}
