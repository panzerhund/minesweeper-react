import {Reducer} from "../reducers/Reducer";
import {BoardState} from "./BoardState";
import {FieldContent} from "./field/Fields";
import {RevealFieldAction} from "./revealFieldAction";

export class RevealFieldReducer implements Reducer<BoardState, RevealFieldAction> {
    public reduce(state: BoardState, action: RevealFieldAction): BoardState {
        const {x, y} = action.field;
        const newState: BoardState = {...state} as BoardState;
        newState.fieldsContents = [...state.fieldsContents];
        newState.fieldsContents[y][x] = FieldContent.REVEALED;
        return newState;
    }
}