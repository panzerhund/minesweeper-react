import {FieldContent} from "./field/Fields";

export interface BoardState {
    rows: number;
    cols: number;
    fieldsContents: FieldContent[][];
}