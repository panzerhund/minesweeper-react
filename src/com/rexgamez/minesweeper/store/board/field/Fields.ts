export enum FieldVisibility {
    EMPTY,
    BOMB,
    REVEALED,
}

export enum FieldContent {
    EMPTY,
    BOMB,
    REVEALED,
}
