import {BoardState} from "../board/BoardState";
import {CHANGE_BOARD_SIZE_ACTION} from "../board/changeBoardSizeAction";
import {ChangeBoardSizeReducer} from "../board/changeBoardSizeReducer";
import {AbstractReducer} from "../reducers/AbstractReducer";
import {FieldContent} from "./field/Fields";
import {REVEAL_FIELD_ACTION} from "./revealFieldAction";
import {RevealFieldReducer} from "./RevealFieldReducer";

export class BoardReducer extends AbstractReducer<BoardState> {
    constructor() {
        super();
        this.initState();
        this.reducers[CHANGE_BOARD_SIZE_ACTION] = new ChangeBoardSizeReducer();
        this.reducers[REVEAL_FIELD_ACTION] = new RevealFieldReducer();
    }

    private initState(): void {
        const cols: number = 10;
        const rows: number = 15;
        this.initialState = {
            cols: cols,
            rows: rows,
            fieldsContents: [],
        };

        for (let i: number = 0; i < rows; i++) {
            this.initialState.fieldsContents[i] = [];
            for (let j: number = 0; j < cols; j++) {
                this.initialState.fieldsContents[i].push(FieldContent.EMPTY);
            }
        }
    }

}
