import {Action} from "redux";
import {FieldData} from "./FieldData";

export const REVEAL_FIELD_ACTION: string = "revealFieldAction";

export interface RevealFieldAction extends Action {
    field: FieldData;
}

export function revealFieldAction(field: FieldData): RevealFieldAction {
    return {
        type: REVEAL_FIELD_ACTION,
        field: field,
    }
}