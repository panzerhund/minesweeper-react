import {Reducer} from "../reducers/Reducer";
import {BoardState} from "./BoardState";
import {ChangeBoardSizeAction} from "./changeBoardSizeAction";

export class ChangeBoardSizeReducer implements Reducer<BoardState, ChangeBoardSizeAction> {
    public reduce(state: BoardState, action: ChangeBoardSizeAction): BoardState {
        return {...state, ...{cols: action.size.x}};
    }
}
