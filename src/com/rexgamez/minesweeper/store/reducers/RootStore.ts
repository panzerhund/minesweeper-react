import {BoardState} from "../board/BoardState";

export interface RootStore {
    board: BoardState;
}