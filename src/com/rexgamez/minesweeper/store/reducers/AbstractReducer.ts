import {Reducer} from "./Reducer";
import {Action} from "redux";

export abstract class AbstractReducer<S> {
    protected initialState: S;
    protected reducers: { [action: string]: Reducer<S, Action> } = {};

    public reduce(state: S = this.initialState, action: Action): S {
        const reducer = this.reducers[action.type];
        if (reducer) {
            return reducer.reduce(state, action);
        }
        return state;
    }
}