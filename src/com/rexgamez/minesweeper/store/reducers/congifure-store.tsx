import {connect} from 'react-redux'
import App from "../../../../../App";
import {RootStore} from "./RootStore";

// const Counter = ...

const mapStateToProps = (state: RootStore) => {
    return {
        board: state.board
    }
}

const mapDispatchToProps = {};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(App)