import {combineReducers, Reducer} from 'redux';
import {RootStore} from './RootStore';
import {BoardReducer} from "../board/BoardReducer";

export const rootReducer: Reducer<RootStore> = combineReducers<RootStore>({
    board: (state: any, action: any) => new BoardReducer().reduce(state, action),
});
