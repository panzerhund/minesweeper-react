import {Action} from "redux";

export interface Reducer<S, A extends Action> {
    reduce(state: S, action: A): S;
}